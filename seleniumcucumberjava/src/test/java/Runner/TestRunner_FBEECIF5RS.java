package Runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features="./src/main/resources/FBEECIF5RS.feature",
        glue={"StepDefinitions"},
        monochrome = true,
        plugin = {"pretty","json:target/surefire-reports/report_FBEECIF5RS.json", "html:target/surefire-reports/report_FBEECIF5RS.html"}
)
public class TestRunner_FBEECIF5RS {
}

