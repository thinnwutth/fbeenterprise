package Runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features="./src/main/resources/FBEPersonal.feature",
        glue={"StepDefinitions"},
        monochrome = true,
        plugin = {"pretty","json:target/surefire-reports/report_FBEPersonal.json", "html:target/surefire-reports/report_FBEPersonal.html"}
)
public class TestRunner_FBEPersonal{
}

