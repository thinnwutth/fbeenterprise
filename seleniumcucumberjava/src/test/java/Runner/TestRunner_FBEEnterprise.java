package Runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features="./src/main/resources/FBEEnterprise.feature",
        glue={"StepDefinitions"},
        monochrome = true,
        plugin = {"pretty","json:target/surefire-reports/report_FBEEnterprise.json", "html:target/surefire-reports/report_FBEEnterprise.html"}
)
public class TestRunner_FBEEnterprise {
}

