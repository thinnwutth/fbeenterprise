package Runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features="./src/main/resources/TickForContact.feature",
        glue={"StepDefinitions"},
        monochrome = true,
        plugin = {"pretty","json:target/surefire-reports/report_TickForContact.json", "html:target/surefire-reports/report_TickForContact.html"}
)
public class TestRunner_TickForContact {
}

