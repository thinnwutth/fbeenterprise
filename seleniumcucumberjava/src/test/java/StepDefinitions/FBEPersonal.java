package StepDefinitions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

import java.io.IOException;

public class FBEPersonal {
    public ActionWords actionwords = new ActionWords();

    @And("^enter NRC: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterNRC(String NRC1, String NRC2,String NRC3, String NRC4) {
    actionwords.enterNRC(NRC1,NRC2,NRC3,NRC4);
    }
    @And("^enter National Identifier Type: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterNationalIdentifierType(String NRCType, String Nationality, String Title, String FirstName, String MiddleName, String LastName, String Gender, String Residence, String StateResidence, String DOB, String EmpStatus, String FatherName) {
        actionwords.enterNationalIdentifierType(NRCType, Nationality, Title, FirstName, MiddleName, LastName, Gender,Residence,StateResidence, DOB, EmpStatus, FatherName);
    }

    @And("^enter MonthlyIncomeLevel \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterMonthlyIncomeLevel(String MonthlyIncomeLevel, String Occupation) {
        actionwords.enterMonthlyIncomeLevel(MonthlyIncomeLevel, Occupation);
    }

    @And("^enter risk for personal: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterriskforpersonal(String riskLevel, String CustomerSegment, String BurmeseName, String IndustryType) {
        actionwords.enterriskforpersonal(riskLevel, CustomerSegment, BurmeseName, IndustryType);
    }
    @And("print PCIF")
    public void printPCIF() {
        actionwords.printPCIF();
    }
    @And("approval process by PCIF creator")
    public void approvalprocessbyPCIFcreator() {
        actionwords.approvalprocessbyPCIFcreator();
    }



}
