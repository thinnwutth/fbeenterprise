package StepDefinitions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

import java.io.IOException;

public class FBEECIF5RS {
    public ActionWords actionwords = new ActionWords();

    @And("^enter relationship2: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterrelationship2(String FromDate, String RelatedParty, String RelationshipType) {
        actionwords.enterrelationship2(FromDate, RelatedParty, RelationshipType);
    }

}
