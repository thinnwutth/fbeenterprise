package StepDefinitions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;

public class FBEEnterprise {
    public ActionWords actionwords = new ActionWords();

    @Given("Navigate to FBE login screen")
    public void NavigatetoFBEloginscreen() throws IOException {
        actionwords.NavigatetoFBEloginscreen();
    }

    @When("^enter Ycode: \"([^\"]*)\"$")
    public void enterYcode(String userName) {
        actionwords.enterYcode(userName);
    }

    @And("^enter FBE password: \"([^\"]*)\"$")
    public void enterFBEpassword(String password) {
        actionwords.enterFBEpassword(password);
    }

    @And("Click Sign In button")
    public void ClickSigninbutton() {
        actionwords.ClickSigninbutton();
    }

    @And("^Type Quick Party Onboarding menu: \"([^\"]*)\"$")
    public void typequickpartyonboardingmenu(String menu) {
        actionwords.typequickpartyonboardingmenu(menu);
    }

    @And("^select Party Type: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void selectPartyType(String PartyType, String PartySubType, String PartyCategory) {
        actionwords.selectPartyType(PartyType,PartySubType,PartyCategory);
    }
    @And("Click Next button")
    public void ClickNextbutton() {
        actionwords.ClickNextbutton();
    }

    @And("^enter RegNum: \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterRegNum(String RegName, String RegNumber) {
        actionwords.enterRegNum(RegName,RegNumber);
    }

    @And("Click Next1 button")
    public void ClickNext1button() {
        actionwords.ClickNext1button();
    }

    @And("tick internet and mobile")
    public void tickinternetandmobile() {
        actionwords.tickinternetandmobile();
    }

    @And("^enter StateReg: \"([^\"]*)\"$")
    public void enterStateReg(String StateReg) {
        actionwords.enterStateReg(StateReg);
    }

    @And("^enter Short Name: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterShortName(String ShortName, String AddType, String CountryRegion) {
        actionwords.enterShortName(ShortName, AddType, CountryRegion);
    }

    @And("Click on Record Address button")
    public void ClickonRecordAddressbutton() {
        actionwords.ClickonRecordAddressbutton();
    }

    @And("^enter address: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enteraddress(String add1, String add2, String add3, String add4, String state, String District, String TownShip, String ContactType, String Email, String ConfirmEmail, String mobiletype, String countryCode, String number, String DocumentCollectionStatus) {
        actionwords.enteraddress(add1,add2, add3, add4, state, District, TownShip, ContactType, Email, ConfirmEmail, mobiletype, countryCode, number, DocumentCollectionStatus);
    }

    @And("^enter relationship1: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterrelationship1(String FromDate, String RelatedParty, String RelationshipType) {
        actionwords.enterrelationship1(FromDate, RelatedParty, RelationshipType);
    }

    @And("^enter risk: \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void enterrisk(String riskLevel, String CustomerSegment, String BurmeseName, String IndustryType) {
        actionwords.enterrisk(riskLevel, CustomerSegment, BurmeseName, IndustryType);
    }

    @And("^enter AnnualTurnOver \"([^\"]*)\"$")
    public void enterAnnualTurnover(String AnnualTurnover) {
        actionwords.enterAnnualTurnover(AnnualTurnover);
    }

    @And("click proceed")
    public void clickproceed() {
        actionwords.clickproceed();
    }

    @And("print the CIF")
    public void printtheCIF() {
        actionwords.printtheCIF();
    }

    @And("approval process")
    public void approvalprocess() {
        actionwords.approvalprocess();
    }

    @And("approval process by approver")
    public void approvalprocessbyapprover() {
        actionwords.approvalprocessbyapprover();
    }

    @And("approval process by creator")
    public void approvalprocessbycreator() {
        actionwords.approvalprocessbycreator();
    }


}
