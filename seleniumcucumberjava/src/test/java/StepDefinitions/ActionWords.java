package StepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.awt.SystemColor.menu;

public class ActionWords {
    public static String browser;
    static WebDriver driver;
    Duration timeout;

    public static void main(String[] args) throws IOException {

    }
    @Before
    public void setBrowserConfig() {
        String projectLocation = System.getProperty("user.dir");
        System.out.println("Project Location" + projectLocation);
        //System.setProperty("webdriver.chrome.driver", projectLocation + "/chromedriver_linux64/chromedriver");
        System.setProperty("webdriver.chrome.driver", projectLocation + "/chromedriver_win32/chromedriver.exe");
        //System.setProperty("webdriver.chrome.driver", projectLocation + "/chromedriver_mac64/chromedriver");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    public void NavigatetoFBEloginscreen(){
        driver.get("http://192.168.128.132:9080/uxp/rt/html/login.html?locale=en-gb");
       //driver.get("http://192.168.93.202:9080/uxp/rt/html/login.html?locale=en-gb");
        driver.manage().deleteAllCookies();
    }

    public void setWait(){
        timeout = Duration.ofSeconds(30);

    }

    public void setTimeout(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void enterYcode(String userName) {
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username']")));
        element.sendKeys(userName);

    }

    public void enterFBEpassword(String password) {
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='password']")));
        element.sendKeys(password);
    }

    public void ClickSigninbutton() {
        /*
        WebElement clickable =  driver.findElement(By.xpath("//input[@id='login']"));
        clickable.click();
        setTimeout();

         */
        setWait();
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        WebElement element = driver.findElement(By.xpath("//input[@id='login']"));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        setTimeout();
    }

    public void typequickpartyonboardingmenu(String menu) {
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='uxpMenuSearch']")));
        element.sendKeys(menu);
        setTimeout();
        element.sendKeys(Keys.RETURN);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void selectPartyType(String PartyType, String PartySubType, String PartyCategory) {
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@id, 'PartyBasicDtls:partyType')][count(. | //*[@role = 'textbox']) = count(//*[@role = 'textbox'])]")));
        element.sendKeys(PartyType);
        element.sendKeys(Keys.RETURN);
        setTimeout();

        setWait();
        WebElement element1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@id, 'PartyBasicDtls:partySubType')][count(. | //*[@role = 'textbox']) = count(//*[@role = 'textbox'])]")));
        element1.sendKeys(PartySubType);
        element1.sendKeys(Keys.RETURN);
        setTimeout();

        setWait();
        WebElement element2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@id, 'PartyBasicDtls:partyCategory')][count(. | //*[@role = 'textbox']) = count(//*[@role = 'textbox'])]")));
        element2.sendKeys(PartyCategory);
        setTimeout();
        element2.sendKeys(Keys.RETURN);

        //setTimeout();
        //WebElement clickable =  driver.findElement(By.xpath("//div[contains(@id, '_HiddenVector_PartyBasicDtls:PartyLineOfBusinessDtls:PartyLineOfBusinessDetail')][count(. | //*[@aria-label = 'Row 2, multiple selection']) = count(//*[@aria-label = 'Row 2, multiple selection'])]"));
        //clickable.click();
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        WebElement element3 = driver.findElement(By.xpath("//div[contains(@id, '_HiddenVector_PartyBasicDtls:PartyLineOfBusinessDtls:PartyLineOfBusinessDetail')][count(. | //*[@aria-label = 'Row 2, multiple selection']) = count(//*[@aria-label = 'Row 2, multiple selection'])]"));
        wait.until(ExpectedConditions.elementToBeClickable(element3));
        element3.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void ClickNextbutton() {
        setWait();
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        WebElement element = driver.findElement(By.xpath("//*[@name = 'Next' and @type = 'submit']"));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        /*
        WebElement clickable =  driver.findElement(By.xpath("//*[@name = 'Next' and @type = 'submit']"));
        clickable.click();
         */
        setTimeout();

    }

    public void enterRegNum(String RegName, String RegNumber) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='dojox_grid__View_9']/div/div/div/div[1]/table/tbody/tr/td[2]")));

        actions.moveToElement(element).click().sendKeys(RegName).build().perform();
        setTimeout();

        setWait();
        WebElement element1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='dojox_grid__View_9']/div/div/div/div[2]/table/tbody/tr/td[2]")));
        actions.moveToElement(element1).click().sendKeys(RegNumber).build().perform();
        setTimeout();
    }

    public void ClickNext1button() {
        /*
        WebElement clickable =  driver.findElement(By.xpath("//*[@name = 'Next' and @type = 'submit']"));
        clickable.click();


         */
        setWait();
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        WebElement element = driver.findElement(By.xpath("//*[@name = 'Next' and @type = 'submit']"));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void tickinternetandmobile() {

        WebElement clickable =  driver.findElement(By.xpath("//input[contains(@id, 'isInternet')]"));
        clickable.click();
        WebElement clickable1 =  driver.findElement(By.xpath("//input[contains(@id, 'isMobile')]"));
        clickable1.click();
        setTimeout();

        /*
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        WebElement element = driver.findElement(By.xpath("//input[contains(@id, 'isInternet')]"));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        setTimeout();

        setWait();
        WebElement element1 = driver.findElement(By.id("//input[contains(@id, 'isMobile')]"));
        wait.until(ExpectedConditions.elementToBeClickable(element1));
        element1.click();
        setTimeout();

         */

    }

    public void enterStateReg(String StateReg) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//tr[10]/td[3]/div[2]/div[3]/input)[starts-with(@id, 'CreateEntPartyBasicDtlsRq')]")));
        actions.moveToElement(element).click().sendKeys(StateReg).build().perform();

    }

    public void enterShortName(String ShortName, String AddType, String CountryRegion) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'shortName')]")));
        actions.moveToElement(element1).click().sendKeys(ShortName).build().perform();

        setWait();
        WebElement element2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'addressType')]")));
        actions.moveToElement(element2).click().sendKeys(AddType).build().perform();

        setWait();
        WebElement element3 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'PartyAddrDtl:country')]")));
        actions.moveToElement(element3).click().sendKeys(CountryRegion).build().perform();
    }

    public void ClickonRecordAddressbutton() {
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ARROW_DOWN).build().perform();
        setWait();
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'RecordAddress_')]")));
        clickable.click();
        setTimeout();
    }

    public void enteraddress(String add1, String add2, String add3, String add4, String state, String District, String TownShip, String ContactType, String Email, String ConfirmEmail, String mobiletype, String countryCode, String number, String DocumentCollectionStatus) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[2]/form/div/div[4]/div[2]/input[contains(@id, 'PartyAddrDtl:addressLine1')]")));
        actions.moveToElement(element).click().sendKeys(add1).build().perform();
        //setTimeout();

        setWait();
        WebElement element1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[6]/div[2]/input[contains(@id, 'PartyAddrDtl:addressLine2')]")));
        actions.moveToElement(element1).click().sendKeys(add2).build().perform();
        //setTimeout();

        setWait();
        WebElement element2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[8]/div[2]/input[contains(@id, 'PartyAddrDtl:addressLine3')]")));
        actions.moveToElement(element2).click().sendKeys(add3).build().perform();
        //setTimeout();

        setWait();
        WebElement element3 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[2]/form/div/div[10]/div[2]/input[contains(@id, 'PartyAddrDtl:addressLine4')]")));
        actions.moveToElement(element3).click().sendKeys(add4).build().perform();
        //setTimeout();

        setWait();
        WebElement element4 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[17]/div[3]/input[contains(@id, 'PartyAddrDtl:addressLine10')]")));
        actions.moveToElement(element4).click().sendKeys(state).build().perform();
        element4.sendKeys(Keys.TAB);
        setTimeout();

        setWait();
        WebElement element5 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[20]/div[3]/input[contains(@id, 'PartyAddrDtl:addressLine5')]")));
        actions.moveToElement(element5).click().sendKeys(District).build().perform();
        element5.sendKeys(Keys.TAB);
        setTimeout();

        setWait();
        WebElement element6 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[23]/div[3]/input[contains(@id, 'PartyAddrDtl:townOrCity')]")));
        actions.moveToElement(element6).click().sendKeys(TownShip).sendKeys(Keys.TAB).sendKeys(Keys.RETURN).build().perform();
        setTimeout();

        setWait();
        WebElement element7 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[starts-with(@id, 'ContactLocation')]")));
        actions.moveToElement(element7).click().sendKeys(ContactType).build().perform();
        //element7.sendKeys(Keys.TAB);
        setTimeout();

        setWait();
        WebElement element8 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'ContactNumber')]")));
        actions.moveToElement(element8).click().sendKeys(Email).build().perform();
        //element8.sendKeys(Keys.TAB);
        setTimeout();

        setWait();
        WebElement element9 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[starts-with(@id, 'ConfirmEmail')]")));
        actions.moveToElement(element9).click().sendKeys(ConfirmEmail).build().perform();
        //element9.sendKeys(Keys.TAB);
        setTimeout();

        setWait();
        WebElement element10 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'contactTypeForMobile')]")));
        actions.moveToElement(element10).click().sendKeys(mobiletype).build().perform();
        //element10.sendKeys(Keys.TAB);
        setTimeout();

        setWait();
        WebElement element11 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'countryCode')]")));
        actions.moveToElement(element11).click().sendKeys(countryCode).build().perform();
        //element11.sendKeys(Keys.TAB);
        setTimeout();


        setWait();
        WebElement element12 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'mobileNumber')]")));
        actions.moveToElement(element12).click().sendKeys(number).build().perform();
        //element12.sendKeys(Keys.TAB);

        setTimeout();

        setWait();
        WebElement element13 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'DocumentStatus')]")));
        actions.moveToElement(element13).click().sendKeys(DocumentCollectionStatus).build().perform();
        //element13.sendKeys(Keys.TAB);
        //setTimeout();

    }

    public void enterrisk(String riskLevel, String CustomerSegment, String BurmeseName, String IndustryType) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_RISKLEVEL_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]")));
        actions.moveToElement(element).click().sendKeys(riskLevel).build().perform();
        setTimeout();

        setWait();
        WebElement element2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_CUSTOMERSEGMENT_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]")));
        actions.moveToElement(element2).click().sendKeys(CustomerSegment).build().perform();
        setTimeout();

        setWait();
        WebElement element3 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id,'UDF_BENTNAME_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]")));
        actions.moveToElement(element3).click().sendKeys(BurmeseName).build().perform();
        setTimeout();

        setWait();
        WebElement element4 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_PARTYMAINSECTOR_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]")));
        actions.moveToElement(element4).click().sendKeys(IndustryType).build().perform();
        setTimeout();

        WebElement element25 = driver.findElement(By.xpath("//input[starts-with(@id, 'UDF_BENTNAME_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]"));
        String value25= element25.getAttribute("value");
        System.out.println("Enterprise Name : " + value25);

    }

    public void enterriskforpersonal(String riskLevel, String CustomerSegment, String BurmeseName, String IndustryType) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_RISKLEVEL_PT_PFN_PERSONALDETAILSUSEREXTN')]")));
        actions.moveToElement(element).click().sendKeys(riskLevel).build().perform();
        setTimeout();

        setWait();
        WebElement element2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_CUSTOMERSEGMENT_PT_PFN_PERSONALDETAILSUSEREXTN')]")));
        actions.moveToElement(element2).click().sendKeys(CustomerSegment).build().perform();
        setTimeout();

        setWait();
        WebElement element3 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_BPARTYNAME_PT_PFN_PERSONALDETAILSUSEREXTN')]")));
        actions.moveToElement(element3).click().sendKeys(BurmeseName).build().perform();
        setTimeout();

        setWait();
        WebElement element4 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_PARTYMAINSECTOR_PT_PFN_PERSONALDETAILSUSEREXTN')]")));
        actions.moveToElement(element4).click().sendKeys(IndustryType).build().perform();
        setTimeout();

    }

    public void enterAnnualTurnover(String AnnualTurnover) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id,'UDF_ANNUALTURNOVER_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]")));
        actions.moveToElement(element1).click().sendKeys(AnnualTurnover).build().perform();
        setTimeout();
        element1.sendKeys(Keys.TAB,Keys.TAB);

    }

    public void enterMonthlyIncomeLevel(String MonthlyIncomeLevel, String Occupation) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_INCOMELEVEL_PT_PFN_PERSONALDETAILSUSEREXTN')]")));
        actions.moveToElement(element1).click().sendKeys(MonthlyIncomeLevel).build().perform();

        setWait();
        WebElement element2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'UDF_OCCUPATIONTITLE_PT_PFN_PERSONALDETAILSUSEREXTN')]")));
        actions.moveToElement(element2).click().sendKeys(Occupation).build().perform();
        setTimeout();

    }

    public void printtheCIF() {

        WebElement element = driver.findElement(By.xpath("//input[contains(@id,'CreateEntPartyBasicDtlsRq:createEntPartyBasicDtlsInput:partyBasicDtls:partyId')]"));
        String value = element.getAttribute("value");
        System.out.println("CIF : " + value);
        setTimeout();

        WebElement element1 = driver.findElement(By.xpath("//input[starts-with(@id, 'CreateEntPartyBasicDtlsRq:createEntPartyBasicDtlsInput:partyBasicDtls:partyType')]"));
        String value1 = element1.getAttribute("value");
        System.out.println("Party Type : " + value1);

        WebElement element2 = driver.findElement(By.xpath("//input[starts-with(@id, 'CreateEntPartyBasicDtlsRq:createEntPartyBasicDtlsInput:partyBasicDtls:partySubType')]"));
        String value2 = element2.getAttribute("value");
        System.out.println("Party Sub-Type : " + value2);

        WebElement element3 = driver.findElement(By.xpath("//input[starts-with(@id, 'CreateEntPartyBasicDtlsRq:createEntPartyBasicDtlsInput:partyBasicDtls:partyCategory')]"));
        String value3 = element3.getAttribute("value");
        System.out.println("Party Category : " + value3);

        //Register number
        WebElement element4 = driver.findElement(By.xpath("//input[starts-with(@id, 'EntFullname')]"));
        String value4 = element4.getAttribute("value");
        System.out.println("Party Category : " + value4);

        WebElement element5 = driver.findElement(By.xpath("//input[starts-with(@id, 'CreateEntPartyBasicDtlsRq:createEntPartyBasicDtlsInput:entPtyBasicDtls:regNumber')]"));
        String value5 = element5.getAttribute("value");
        System.out.println("Register Number : " + value5);

        WebElement element6 = driver.findElement(By.xpath("//input[starts-with(@id, 'shortName')]"));
        String value6 = element6.getAttribute("value");
        System.out.println("Short Name : " + value6);

        WebElement element7 = driver.findElement(By.xpath("//input[starts-with(@id, 'CreateEntPartyBasicDtlsRq:createEntPartyBasicDtlsInput:entPtyBasicDtls:regCountry')]"));
        String value7 = element7.getAttribute("value");
        System.out.println("Reg Country : " + value7);

        WebElement element8 = driver.findElement(By.xpath("//input[starts-with(@id, 'CreateEntPartyBasicDtlsRq:createEntPartyBasicDtlsInput:entPtyBasicDtls:regState')]"));
        String value8 = element8.getAttribute("value");
        System.out.println("Reg State : " + value8);

        WebElement element9 = driver.findElement(By.xpath("//input[starts-with(@id, 'PartyAddrSpecDtl:addressType')]"));
        String value9= element9.getAttribute("value");
        System.out.println("Address Type : " + value9);

        WebElement element10 = driver.findElement(By.xpath("//input[starts-with(@id, 'PartyAddrDtl:country')]"));
        String value10= element10.getAttribute("value");
        System.out.println("country : " + value10);

        WebElement element11 = driver.findElement(By.xpath("//input[starts-with(@id, 'AddressConcatenated')]"));
        String value11= element11.getAttribute("value");
        System.out.println("Address : " + value11);

        WebElement element12 = driver.findElement(By.xpath("//input[starts-with(@id, 'ContactLocation')]"));
        String value12= element12.getAttribute("value");
        System.out.println("Email Contact Type : " + value12);

        WebElement element13 = driver.findElement(By.xpath("//input[starts-with(@id, 'ContactNumber')]"));
        String value13= element13.getAttribute("value");
        System.out.println("Email : " + value13);

        WebElement element14 = driver.findElement(By.xpath("//input[starts-with(@id, 'ConfirmEmail')]"));
        String value14= element14.getAttribute("value");
        System.out.println("Confirm Email : " + value14);

        WebElement element15 = driver.findElement(By.xpath("//input[starts-with(@id, 'contactTypeForMobile')]"));
        String value15= element15.getAttribute("value");
        System.out.println("Mobile Type : " + value15);

        WebElement element16 = driver.findElement(By.xpath("//input[starts-with(@id, 'countryCode')]"));
        String value16= element16.getAttribute("value");
        System.out.println("Country code: " + value16);

        WebElement element17 = driver.findElement(By.xpath("//input[starts-with(@id, 'mobileNumber')]"));
        String value17= element17.getAttribute("value");
        System.out.println("Number: " + value17);

        WebElement element18 = driver.findElement(By.xpath("//input[starts-with(@id, 'DocumentStatus')]"));
        String value18= element18.getAttribute("value");
        System.out.println("Document Status : " + value18);

        /*
        WebElement element19 = driver.findElement(By.xpath("//div[@id='dojox_grid__View_13']/div/div/div/div/table/tbody/tr/td[3]"));
        String value19= element19.getAttribute("value");
        System.out.println("Related PCIF : " + value19);

        WebElement element20 = driver.findElement(By.xpath("//div[@id='dojox_grid__View_13']/div/div/div/div/table/tbody/tr/td[2]"));
        String value20= element20.getAttribute("value");
        System.out.println("Client Trustee : " + value20);

        WebElement element21 = driver.findElement(By.xpath("//div[@id='dojox_grid__View_13']/div/div/div/div/table/tbody/tr/td[4]"));
        String value21= element21.getAttribute("value");
        System.out.println("PCIF Name : " + value21);
         */

        WebElement element22 = driver.findElement(By.xpath("//input[starts-with(@id, 'UDF_RISKLEVEL_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]"));
        String value22= element22.getAttribute("value");
        System.out.println("Risk : " + value22);

        WebElement element23 = driver.findElement(By.xpath("//input[starts-with(@id, 'UDF_ANNUALTURNOVER_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]"));
        String value23= element23.getAttribute("value");
        System.out.println("Annual Turn Over : " + value23);

        WebElement element24 = driver.findElement(By.xpath("//input[starts-with(@id, 'UDF_CUSTOMERSEGMENT_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]"));
        String value24= element24.getAttribute("value");
        System.out.println("Customer Segment : " + value24);


        WebElement element26 = driver.findElement(By.xpath("//input[starts-with(@id, 'UDF_PARTYMAINSECTOR_PT_PFN_ENTERPRISEDETAILSUSEREXTN')]"));
        String value26= element26.getAttribute("value");
        System.out.println("Industry Type : " + value26);
    }

    public void printPCIF() {

        WebElement element = driver.findElement(By.xpath("//input[@name = '03createPersonalPartyRq:createPersonPartyInput:partyBasicDtls:partyId' and @type = 'text']"));
        String value = element.getAttribute("value");
        System.out.println("PCIF : " + value);
        setTimeout();
    }

    public void clickproceed() {
        setWait();
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'Next')]")));
        clickable.click();
        setTimeout();

        setWait();
        WebElement clickable1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'Proceed_')]")));
        clickable1.click();
        setTimeout();

        setWait();
        WebElement clickable2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'AskForApproval')]")));
        clickable2.click();
        setTimeout();
    }

    public void enterrelationship1(String FromDate, String RelatedParty, String RelationshipType) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@id, 'addRow_HiddenVector_MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls')]")));
        clickable.click();
        setTimeout();

        setWait();
        WebElement element15 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls:partyRelationDtl:dateFrom')]")));
        actions.moveToElement(element15).click().sendKeys(FromDate).build().perform();
        setTimeout();

        setWait();
        WebElement element16 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id,'MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls:partyRelationDtl:relatedParty')]")));
        actions.moveToElement(element16).click().sendKeys(RelatedParty).sendKeys(Keys.TAB).build().perform();
        setTimeout();

        WebElement element21 = driver.findElement(By.xpath("//input[starts-with(@id, 'MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls:partyRelationDtl:relatedPartyName')]"));
        String value21= element21.getAttribute("value");
        System.out.println("PCIF Name : " + value21);

        setWait();
        WebElement element17 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id,'MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls:partyRelationDtl:relationship')]")));
        actions.moveToElement(element17).click().sendKeys(RelationshipType).build().perform();
        setTimeout();

        setWait();
        WebElement clickable1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@id,'saveRow_HiddenVector_MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls')]")));
        clickable1.click();
        setTimeout();

        WebElement addedRow = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@id, '_HiddenVector_MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls')]")));
        List<WebElement> rows = addedRow.findElements(By.xpath("//div[@id='dojox_grid__View_13']/div/div/div/div/table/tbody/tr"));
        System.out.println("rows:" + rows.size());
        String rowText = addedRow.getText();
        System.out.println("Row Text: " + rowText);

    }

    public void enterrelationship2(String FromDate, String RelatedParty, String RelationshipType) {
        Actions actions = new Actions(driver);
        // create a list of values to iterate over
        List<String[]> valuesList = new ArrayList<>();
        valuesList.add(new String[]{FromDate, RelatedParty, RelationshipType});
        valuesList.add(new String[]{"2023-05-09", "Jane Doe", "Sister"});

        // loop through the values and execute the code block for each set of values
        for (String[] values : valuesList) {
            setWait();
            WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@id, 'addRow_HiddenVector_MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls')]")));
            clickable.click();

            setWait();
            WebElement element15 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls:partyRelationDtl:dateFrom')]")));
            actions.moveToElement(element15).click().sendKeys(values[0]).build().perform();

            setWait();
            WebElement element16 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id,'MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls:partyRelationDtl:relatedParty')]")));
            actions.moveToElement(element16).click().sendKeys(values[1]).build().perform();

            setWait();
            WebElement element17 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id,'MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls:partyRelationDtl:relationship')]")));
            actions.moveToElement(element17).click().sendKeys(values[2]).build().perform();

            setWait();
            WebElement clickable1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@id,'saveRow_HiddenVector_MaintRelatedPartyRq:maintRelatedPartyInput:partyRelationDtls')]")));
            clickable1.click();
        }

    }

    public void approvalprocess() {
        //click setting
        setWait();
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='uxpHelpMenu']/span[3]")));
        clickable.click();
        setTimeout();
        //click log out
        setWait();
        WebElement clickable1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[@id='logoutButton_text']")));
        clickable1.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void approvalprocessbyapprover() {
        //click notification
        setWait();
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='notification-icon']/span")));
        clickable.click();
        setTimeout();
        //click next
        setWait();
        WebElement clickable1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[3]/div/div[2]/input | //input[contains(@id, 'Next2118804cd582c4mF%')]")));
        clickable1.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //click approve
        setWait();
        WebElement clickable2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[2]/input | //input[contains(@id, 'Approve_22118804cd582c4mF%')]")));
        clickable2.click();
    }

    public void approvalprocessbycreator() {
        //click notification
        setWait();
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='notification-icon']/span")));
        clickable.click();
        setTimeout();
        //click next from process notification screen
        setWait();
        WebElement clickable1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[3]/div/div[2]/input | //input[contains(@id, 'Next211880504e0b4FVA%')]")));
        clickable1.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //click next related task of process notification screen
        setWait();
        WebElement clickable2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[3]/div/div[2]/input | //input[contains(@id, 'Next21188050a9841GeN%')]")));
        clickable2.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //tick checkbox
        Actions actions = new Actions(driver);
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form[@name = 'PT_POB_TestQuickOnBoarding']")));
        actions.moveToElement(element).perform();
        WebElement subElement = driver.findElement(By.xpath("//input[@name = '09Status' and @type = 'checkbox'] | //form/div/div[2]/input"));
        subElement.click();
        setTimeout();

        //click next
        setWait();
        WebElement clickable4 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[starts-with(@id, 'Next')]")));
        clickable4.click();
        setTimeout();

        //click accept radio button
        setWait();
        WebElement element1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form[@name = 'PT_POB_TestQuickOnBoarding']")));
        actions.moveToElement(element1).perform();
        WebElement subElement1 = driver.findElement(By.xpath(" //form/div[4]/input | //input[contains(@id, 'radioAccept')] | //input[@value = 'radioAccept']"));
        subElement1.click();
        setTimeout();

        //click next
        setWait();
        WebElement clickable6 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'Next')]")));
        clickable6.click();
        setTimeout();

        WebElement element2 = driver.findElement(By.xpath("//input[contains(@id, 'ReadEntPartyBasicDtlsRs:readEntPartyBasicDtlsOutput:partyBasicDtls:partyId')]"));
        String value = element2.getAttribute("value");
        System.out.println("Confirm CIF from get approval screen : " + value);
        setTimeout();

        //click next
        setWait();
        WebElement clickable7 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='PT_POB_TestQuickOnBoarding_2']/div[2]/input")));
        clickable7.click();
        setTimeout();

    }
    public void approvalprocessbyPCIFcreator() {
        //click notification
        setWait();
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='notification-icon']/span")));
        clickable.click();
        setTimeout();
        //click next from process notification screen
        setWait();
        WebElement clickable1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[3]/div/div[2]/input | //input[contains(@id, 'Next211880504e0b4FVA%')]")));
        clickable1.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //click next related task of process notification screen
        setWait();
        WebElement clickable2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[3]/div/div[2]/input | //input[contains(@id, 'Next21188050a9841GeN%')]")));
        clickable2.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //tick checkbox
        Actions actions = new Actions(driver);
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form[@name = 'PT_POB_TestQuickOnBoarding']")));
        actions.moveToElement(element).perform();
        WebElement subElement = driver.findElement(By.xpath("//input[@name = '09Status' and @type = 'checkbox'] | //form/div/div[2]/input"));
        subElement.click();
        setTimeout();

        //click next
        setWait();
        WebElement clickable4 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[starts-with(@id, 'Next')]")));
        clickable4.click();
        setTimeout();

        //click next
        setWait();
        WebElement clickable6 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id, 'Next')]")));
        clickable6.click();
        setTimeout();

        /*
        WebElement element2 = driver.findElement(By.xpath("//input[@name = '03ReadPersonPartyRs:readPersonalPartyOutput:partyBasicDtls:partyId']"));
        String value = element2.getAttribute("value");
        System.out.println("Confirm PCIF from get approval screen : " + value);
        setTimeout();

         */

        //click next
        setWait();
        WebElement clickable7 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='PT_POB_TestQuickOnBoarding_2']/div[2]/input")));
        clickable7.click();
        setTimeout();

    }

    //Starting from here this is for creating PCIFs
    public void enterNRC(String NRC1, String NRC2,String NRC3, String NRC4) {
        Actions actions = new Actions(driver);
        setWait();
        WebElement element1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form[@name = 'PT_POB_TestQuickOnBoarding']")));
        actions.moveToElement(element1).perform();
        //click NRC1 and set value
        WebElement subElement1 = driver.findElement(By.xpath("(//input[@value='▼ '])[8]"));
        subElement1.click();
        setWait();
        String NRC1Xpath = "//*/text()[normalize-space(.)='" + NRC1 + "']/parent::*";
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NRC1Xpath)));
        clickable.click();
        setTimeout();

        //click NRC2 and set value
        WebElement subElement2 = driver.findElement(By.xpath("(//input[@value='▼ '])[9]"));
        subElement2.click();
        setWait();
        String NRC2Xpath = "//*/text()[normalize-space(.)='" + NRC2 + "']/parent::*";
        WebElement clickable1 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NRC2Xpath)));
        clickable1.click();
        setTimeout();

        //click NRC3 and set value
        WebElement subElement3 = driver.findElement(By.xpath("(//input[@value='▼ '])[10]"));
        subElement3.click();
        setWait();
        String NRC3Xpath = "//*/text()[normalize-space(.)='" + NRC3 + "']/parent::*";
        WebElement clickable2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NRC3Xpath)));
        clickable2.click();
        setTimeout();

        //click NRC4 and set value
        WebElement subElement4 = driver.findElement(By.xpath("//input[@name = '03nrcText']"));
        subElement4.sendKeys(NRC4);
        setTimeout();
    }
    public void enterNationalIdentifierType(String NRCType,String Nationality, String Title, String FirstName, String MiddleName, String LastName, String Gender, String Residence, String StateResidence, String DOB, String EmpStatus, String FatherName){
        //enter National Identifier Type
        WebElement subElement1 = driver.findElement(By.xpath("(//input[@value='▼ '])[7]"));
        subElement1.click();
        setWait();
        String NRCTypeXpath = "//*/text()[normalize-space(.)='" + NRCType + "']/parent::*";
        WebElement clickable = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(NRCTypeXpath)));
        clickable.click();
        //enter nationality
        WebElement subElement2 = driver.findElement(By.xpath("//input[contains(@id, 'createPersonalPartyRq:createPersonPartyInput:partyPersonDtl:countryNationality')]"));
        subElement2.sendKeys(Nationality);
        subElement2.sendKeys(Keys.RETURN);

        //enter title
        WebElement subElement3 = driver.findElement(By.xpath("(//input[@value='▼ '])[9]"));
        subElement3.click();
        setWait();
        String TitleXpath = "//*/text()[normalize-space(.)='" + Title + "']/parent::*";
        WebElement clickable2 = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(TitleXpath)));
        clickable2.click();

        //enter first name, last name and middle name
        WebElement firstName = driver.findElement(By.xpath("//input[@name = '03namedtl:firstName']"));
        firstName.sendKeys(FirstName);
        WebElement middleName = driver.findElement(By.xpath("//input[@name = '03namedtl:middleNames']"));
        middleName.sendKeys(MiddleName);
        WebElement lastName = driver.findElement(By.xpath("//input[@name = '03namedtl:lastName']"));
        lastName.sendKeys(LastName);

        //select gender
        WebElement gender = driver.findElement(By.xpath("(//input[@value='▼ '])[15]"));
        gender.click();
        setWait();
        String genderXpath = "//*/text()[normalize-space(.)='" + Gender + "']/parent::*";
        WebElement clickGender = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(genderXpath)));
        clickGender.click();

        //select country of residence
        WebElement residence = driver.findElement(By.xpath("(//input[@value='▼ '])[16]"));
        residence.click();
        setWait();
        String residenceXpath = "//*/text()[normalize-space(.)='" + Residence + "']/parent::*";
        WebElement clickResidence = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(residenceXpath)));
        clickResidence.click();

        //select state of residence
        WebElement stateResidence = driver.findElement(By.xpath("(//input[@value='▼ '])[18]"));
        stateResidence.click();
        setWait();
        String stateResidenceXpath = "//*/text()[normalize-space(.)='" + StateResidence + "']/parent::*";
        WebElement clickStateResidence = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(stateResidenceXpath)));
        clickStateResidence.click();

        //enter DOB
        WebElement dob = driver.findElement(By.xpath("//input[contains(@id, 'createPersonalPartyRq:createPersonPartyInput:partyPersonDtl:dateOfBirth')]"));
        dob.sendKeys(DOB);
        dob.sendKeys(Keys.RETURN);

        //enter father name
        WebElement fatherName = driver.findElement(By.xpath("//input[contains(@id, 'createPersonalPartyRq:createPersonPartyInput:partyPersonDtl:fathersName')]"));
        fatherName.sendKeys(FatherName);
        fatherName.sendKeys(Keys.RETURN);
        setTimeout();

        //enter employment status
        WebElement empStatus = driver.findElement(By.xpath("(//input[@value='▼ '])[13]"));
        empStatus.click();
        setWait();
        String empStatusXpath = "//*/text()[normalize-space(.)='" + EmpStatus + "']/parent::*";
        WebElement clickEmpStatus = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(empStatusXpath)));
        clickEmpStatus.click();

    }
    public void MaintainPartyDetail(String CIF){
        setWait();
        WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = '03PartyID']")));
        element.sendKeys(CIF);

    }
    public void clickRelatedItem(String SubMenu){
        //click related item
        WebElement relatedItem = driver.findElement(By.xpath("//*/text()[normalize-space(.)='Related Items']/parent::*"));
        relatedItem.click();
        setWait();
        //click contact details
        String relatedItemXpath = "//*/text()[normalize-space(.)='" + SubMenu + "']/parent::*";
        WebElement clickSubmenu = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(relatedItemXpath)));
        clickSubmenu.click();
        //click use for contact check box
        WebElement tickcheckbox = driver.findElement(By.xpath("//input[@name = '09addPartyContactDtlsRq:addPartyContactDtlsInput:contactDtls:contactDtl:isApplicable' and @type = 'checkbox']"));
        tickcheckbox.click();
        setWait();
        WebElement clickSave = new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'Save Row' and @type = 'button']")));
        clickSave.click();
    }

    @After
    public static void tearDown(){
        //driver.quit();
    }



}


