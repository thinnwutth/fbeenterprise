Feature: FBEEnterprise



  Scenario Outline: Create enterprise customer with one personal relationship
    Given Navigate to FBE login screen
    When enter Ycode: "<username>"
    And enter FBE password: "<password>"
    And Click Sign In button
    And Type Quick Party Onboarding menu: "<menu>"
    And select Party Type: "<PartyType>" and "<PartySubType>" and "<PartyCategory>"
    And Click Next button
    And enter RegNum: "<RegName>" and "<RegNumber>"
    And Click Next1 button
    And enter StateReg: "<StateReg>"
    And enter Short Name: "<ShortName>" and "<AddType>" and "<CountryRegion>"
    And Click on Record Address button
    And enter address: "<add1>" and "<add2>" and "<add3>" and "<add4>" and "<state>" and "<District>" and "<TownShip>" and "<ContactType>" and "<Email>" and "<ConfirmEmail>" and "<mobiletype>" and "<countryCode>" and "<number>" and "<DocumentCollectionStatus>"
    And enter risk: "<riskLevel>" and "<CustomerSegment>" and "<BurmeseName>" and "<IndustryType>"
    And enter AnnualTurnOver "<AnnualTurnover>"
    And print the CIF
    And click proceed
    And approval process
    When enter Ycode: "<username1>"
    And enter FBE password: "<password>"
    And Click Sign In button
    And approval process by approver
    And approval process
    When enter Ycode: "<username>"
    And enter FBE password: "<password1>"
    And Click Sign In button
    And approval process by creator

    Examples:
      | username | username1| password |password1| menu |PartyType|PartySubType|PartyCategory|RegName|RegNumber|StateReg|ShortName|AddType|CountryRegion|add1|add2|add3|add4|state|District|TownShip|ContactType|Email|ConfirmEmail|mobiletype|countryCode|number|DocumentCollectionStatus|FromDate|RelatedParty|RelationshipType|riskLevel|AnnualTurnover|CustomerSegment|BurmeseName|IndustryType|
      | Y009890 | Y008237  | yoma |yoma     | Quick Party Onboarding |Enterprise|Company Limited by Guarantee| Full |Company Name Four|0909090126|Yangon|shortname|Home Address|Myanmar|add line1|add line 2| add line 3| add line 4|Yangon|Yangon East|Yankin|Work|thinwutthmone@yomabank.com|thinwutthmone@yomabank.com|Work|+95|09979355021|Documents not collected|2023-05-09|10662451|Client/Trustee|LOW RISK|More than 50,000,000 MMK|Association|အဆင်|Construction|

