Feature: FBEPersonal



  Scenario Outline: Tick use for contact
    Given Navigate to FBE login screen
    When enter Ycode: "<username>"
    And enter FBE password: "<password>"
    And Click Sign In button
    And Type Quick Party Onboarding menu: "<menu>"
    And Maintain party detail "<CIF>"
    And Click Next button
    And click Related Item "<SubMenu>"

    Examples:
      | username | username1| password |password1| menu |CIF|SubMenu|
      | Y009890 | Y008237  | yoma |yoma     | Maintain Party Details |10665876|Contact Details|


