Feature: FBEECIF1RS



  Scenario Outline: Create enterprise customer with one personal relationship
    Given Navigate to FBE login screen
    When enter Ycode: "<username>"
    And enter FBE password: "<password>"
    And Click Sign In button
    And Type Quick Party Onboarding menu: "<menu>"
    And select Party Type: "<PartyType>" and "<PartySubType>" and "<PartyCategory>"
    And Click Next button
    And enter RegNum: "<RegName>" and "<RegNumber>"
    And Click Next1 button
    And tick internet and mobile
    And enter StateReg: "<StateReg>" and "<ShortName>" and "<AddType>" and "<CountryRegion>"
    And Click on Record Address button
    And enter address: "<add1>" and "<add2>" and "<add3>" and "<add4>" and "<state>" and "<District>" and "<TownShip>" and "<ContactType>" and "<Email>" and "<ConfirmEmail>" and "<mobiletype>" and "<countryCode>" and "<number>" and "<DocumentCollectionStatus>"
    And enter relationship1: "<FromDate>" and "<RelatedParty>" and "<RelationshipType>"
    And enter risk: "<riskLevel>" and "<AnnualTurnover>" and "<CustomerSegment>" and "<EnterpriseName>" and "<IndustryType>"
    And print the CIF
    And click proceed

    Examples:
      | username | password | menu |PartyType|PartySubType|PartyCategory|RegName|RegNumber|StateReg|ShortName|AddType|CountryRegion|add1|add2|add3|add4|state|District|TownShip|ContactType|Email|ConfirmEmail|mobiletype|countryCode|number|DocumentCollectionStatus|FromDate|RelatedParty|RelationshipType|riskLevel|AnnualTurnover|CustomerSegment|EnterpriseName|IndustryType|
      | Y009890 | yoma | Quick Party Onboarding |Enterprise|Company Limited by Guarantee| Full |Company Name Three|090909090|Yangon|shortname|Home Address|Myanmar|add line1|add line 2| add line 3| add line 4|Mandalay|Yangon East|Yankin|Work|thinwutthmone@yomabank.com|thinwutthmone@yomabank.com|Work|+95|09979355021|Documents not collected|2023-05-09|04300063|Client/Trustee|LOW RISK|More than 50,000,000 MMK|Association|အဆင်|Construction|

