Feature: FBEPersonal



  Scenario Outline: Create personal customer
    Given Navigate to FBE login screen
    When enter Ycode: "<username>"
    And enter FBE password: "<password>"
    And Click Sign In button
    And Type Quick Party Onboarding menu: "<menu>"
    And select Party Type: "<PartyType>" and "<PartySubType>" and "<PartyCategory>"
    And Click Next button
    And enter NRC: "<NRC1>" and "<NRC2>" and "<NRC3>" and "<NRC4>"
    And Click Next1 button
    And tick internet and mobile
    And enter National Identifier Type: "<NRCType>" and "<Nationality>" and "<Title>" and "<FirstName>" and "<MiddleName>" and "<LastName>" and "<Gender>" and "<Residence>" and "<StateResidence>" and "<DOB>" and "<EmpStatus>" and "<FatherName>"
    And enter Short Name: "<ShortName>" and "<AddType>" and "<CountryRegion>"
    And Click on Record Address button
    And enter address: "<add1>" and "<add2>" and "<add3>" and "<add4>" and "<state>" and "<District>" and "<TownShip>" and "<ContactType>" and "<Email>" and "<ConfirmEmail>" and "<mobiletype>" and "<countryCode>" and "<number>" and "<DocumentCollectionStatus>"
    And enter risk for personal: "<riskLevel>" and "<CustomerSegment>" and "<BurmeseName>" and "<IndustryType>"
    And enter MonthlyIncomeLevel "<MonthlyIncomeLevel>" and "<Occupation>"
    And print PCIF
    And click proceed
    And approval process
    When enter Ycode: "<username1>"
    And enter FBE password: "<password>"
    And Click Sign In button
    And approval process by approver
    And approval process
    When enter Ycode: "<username>"
    And enter FBE password: "<password1>"
    And Click Sign In button
    And approval process by PCIF creator

    Examples:
      | username | username1| password |password1| menu |PartyType|PartySubType|PartyCategory|NRC1|NRC2|NRC3|NRC4|NRCType|Nationality|Title|FirstName|MiddleName|LastName|Gender|Residence|StateResidence|DOB|EmpStatus|FatherName|ShortName|AddType|CountryRegion|add1|add2|add3|add4|state|District|TownShip|ContactType|Email|ConfirmEmail|mobiletype|countryCode|number|DocumentCollectionStatus|riskLevel|MonthlyIncomeLevel|CustomerSegment|BurmeseName|IndustryType|Occupation|
      | Y009890 | Y008237  | yoma |yoma     | Quick Party Onboarding |Personal|Individual| Full |14|ZaLaNa|(N)|190915| NRC |Myanmar  |U      |Thin     |Wutt      |Hmone   |Female|Myanmar  |Yangon        |1997-10-10|Employed|U Ba|shortname|Home Address|Myanmar|add line1|add line 2| add line 3| add line 4|Yangon|Yangon East|Yankin|Work|thinwutthmone@yomabank.com|thinwutthmone@yomabank.com|Work|+95|09979355021|Documents not collected|LOW RISK|More than 5,000,000 MMK|Consumer|အဆင်|Construction|Employee|

